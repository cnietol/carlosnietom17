﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Droid : MonoBehaviour
{
    #region GameObjects
    [Header("Ragdolls")]
    public GameObject ragdoll;
    public GameObject ragdollnoHead;
    #endregion

    #region Variables
    public int hp { get; set; }

    private bool nodestruido = true;
    public bool headshoot { get; set; }
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        hp = 750;
        headshoot = false;
    }

    // Update is called once per frame
    void Update()
    {
        mort();
        if (headshoot && nodestruido)
        {
            Vector3 vector = new Vector3(this.transform.position.x, this.transform.position.y - 0.5f, this.transform.position.z);
            Instantiate(ragdollnoHead, vector, Quaternion.LookRotation(this.transform.forward), this.transform);
            nodestruido = false;
            this.transform.parent.transform.GetChild(0).gameObject.SetActive(false);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "barril")
        {
            hp = 0;
            mort();
        }
    }
    #region Metodos
    public void mort()
    {
        if (hp <= 0 && nodestruido)
        {
            this.transform.GetChild(3).gameObject.SetActive(true);
            Instantiate(ragdoll, this.transform.position, Quaternion.LookRotation(this.transform.forward), this.transform);
            nodestruido = false;
            this.transform.GetChild(0).transform.gameObject.SetActive(false);
        }
    }
    #endregion
}
