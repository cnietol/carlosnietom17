﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class change : MonoBehaviour
{
    #region Vectores
    private Vector3 pos;
    private Vector3 old;
    #endregion

    #region GameObjects
    private GameObject helis;
    [Header("GameObjects")]
    public GameObject player;
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        helis = GameObject.Find("Helis");
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "player")
        {
            changeGameplay();
            StartCoroutine(final());
        }
    }
    #region Metodos
    public void changeGameplay()
    {
        //Guarda la posicion del player para cuando se acabe volver al mismo sitio que estaba
        old = player.transform.position;
        helis.transform.GetChild(1).gameObject.SetActive(true);
        helis.transform.GetChild(0).gameObject.SetActive(false);
        pos = this.transform.parent.transform.GetChild(5).transform.position;
        this.transform.parent.transform.GetChild(5).gameObject.SetActive(false);
        this.transform.GetComponent<Collider>().enabled = false;
    }
    #endregion

    #region Coorutinas
    IEnumerator final()
    {
        //20 segundos para tirar los recursos
        yield return new WaitForSeconds(20);
        player.transform.position = old;
        helis.transform.GetChild(1).gameObject.SetActive(false);
        this.transform.parent.transform.GetChild(5).gameObject.SetActive(true);
        this.transform.parent.transform.GetChild(5).transform.position = pos;
    }
    #endregion

}
