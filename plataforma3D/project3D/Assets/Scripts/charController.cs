﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class charController : MonoBehaviour
{
    private float speed = 1.5f;
    private float jumpSpeed = 6.0f;
    private float gravity = 9.8f;

    private Vector3 moveDirection = Vector3.zero;


    //Cuando desaparece acaba el juego
    public GameObject player;
    Animator anim;

    
   

    //Booleano para controlar los movimientos si está escalando
    private bool escalando=false;

    //Variable que sirve como referencia para las animaciones. Si es positiva se anda hacia adelante, si no, hacia atras
    float movimiento=0;

    

    // Use this for initialization
    void Start()
    {
        anim = player.GetComponent<Animator>();
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.transform.tag=="escalable")
        {
            escalando = true;
        }

        if (hit.transform.tag == "exitEscalable")
        {
            escalando = false;
        }


    }

    

    // Update is called once per frame
    void Update()
    {

        if (player!=null)
        {
            //Al multiplicarla por la speed, controlamos que ande o corra. Este invento es mio, no es copiado
            movimiento = Input.GetAxis("Vertical") * speed;

            CharacterController controller = GetComponent<CharacterController>();

            if (controller.isGrounded)
            {

                escalando = false;
                moveDirection = new Vector3(0, 0, Input.GetAxis("Vertical"));
                moveDirection = transform.TransformDirection(moveDirection);
                moveDirection *= speed;

                //Saltar
                if (Input.GetButton("Jump"))
                {
                    moveDirection.y = jumpSpeed;
                }

                //Correr
                if (Input.GetKey(KeyCode.LeftShift))
                {
                    speed = 5f;
                    if (movimiento >= 5f)
                    {
                        anim.SetBool("run", true);
                    }
                    else
                    {
                        anim.SetBool("run", false);
                    }

                }
                else
                {
                    speed = 1.5f;
                    anim.SetBool("run", false);
                }

                //Animaciones
                //Andar hacia delante
                if (movimiento > 0 && movimiento < 5)
                {
                    anim.SetBool("walk", true);
                }

                //Andar hacia atras
                if (movimiento < 0)
                {
                    anim.SetBool("walkB", true);
                    speed = 1f;
                }

                //Idle
                if (movimiento == 0)
                {
                    anim.SetBool("walkB", false);
                    anim.SetBool("walk", false);
                }
            }

            //Controles mientras se escala
            if (escalando)
            {
                anim.SetBool("walkB", false);
                anim.SetBool("walk", false);
                anim.SetBool("run", false);

                moveDirection = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0);
                moveDirection = transform.TransformDirection(moveDirection);
                moveDirection *= speed;

                //Saltar
                if (Input.GetButton("Jump"))
                {
                    moveDirection.y = 3;
                    moveDirection.z = -3;
                    this.transform.rotation = new Quaternion(0, 180, 0, 0);
                    escalando = false;
                }

                //Escalar mas rapido
                if (Input.GetKey(KeyCode.LeftShift)) speed = 2f;
                else speed = 1f;
            }

            //Mover al personaje
            controller.Move(moveDirection * Time.deltaTime);

            //No aplicar la gravedad mientras se escala
            if (!escalando)
            {
                moveDirection.y -= gravity * Time.deltaTime;
            }

            //Rotar al jugador en el aire y en el suelo, pero no escalando
            if (!escalando)
            {
                transform.Rotate(0, Input.GetAxis("Horizontal"), 0);
            }
        } 
        

       

    }



}
