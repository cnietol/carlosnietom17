﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraController : MonoBehaviour
{

    //public Transform target;
    private float smoothSpeed = 0.125f;
    private float mouseSensibility = 100f;
    private float xRot;


    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        
    }



    // Update is called once per frame
    void Update()
    {

        float mouseX = Input.GetAxis("Mouse X")*mouseSensibility*Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensibility * Time.deltaTime;
        
        transform.eulerAngles = new Vector3(mouseX,mouseY,0);
        

    }

    
}
