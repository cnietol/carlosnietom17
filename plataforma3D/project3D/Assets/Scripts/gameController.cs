﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class gameController : MonoBehaviour
{
    public Text text;
    public Text progreso;
    private float contador;
    public GameObject player;
    public Light antorcha1;
    public Light antorcha2;
    public ranking rank;
    public Text ranking;

    // Start is called before the first frame update
    void Start()
    {
        text.text = "60";
        contador = 60f;
        ranking.text = "Mejor puntuacion: " + rank.puntuacion;
    }

    // Update is called once per frame
    void Update()
    {
        if (antorcha1.enabled == false || antorcha2.enabled == false && contador>0)
        {
            contador -= 1 * Time.deltaTime;
        }

        if (antorcha1.enabled==true && antorcha2.enabled == true)
        {
            progreso.text = "Antorchas encendidas: 2";
            rank.puntuacion = contador;
            ranking.text ="Mejor puntuacion: "+contador.ToString();
            
        } else if (antorcha1.enabled == true || antorcha2.enabled == true)
        {
            progreso.text = "Antorchas encendidas: 1";
        } else
        {
            progreso.text = "Antorchas encendidas: 0";
        }

        text.text = contador.ToString();

        if (contador<=0 && antorcha1.enabled == false && antorcha2.enabled == false)
        {
            Destroy(player.gameObject);
        }

    }
}
