﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cuevasController : MonoBehaviour {

    int cols;
    int rows;

	private GameObject currentBlockType;
	public GameObject[] blockTypes;
    public GameObject water;
    public GameObject tree;

    [Tooltip("True for minecraft style voxels")]
	public bool SnapToGrid = true;

    public int seed = 200;
	public float amp = 10f;
	public float freq = 10f;

	private Vector3 myPos;

    //Variables para el desplazamiento    

    public GameObject player;
    Vector3 terrainPos = Vector3.zero;
    Vector3 playerPos = Vector3.zero;
    Vector3 difference = Vector3.zero;



    void Start () {
        //el mapa de perlin siempre devuelve la misma posicion.
        //si tu quieres que devuelva algo distinto tendras que hacer un desfase. Ese defase es conocido como "seed" (semilla)
        //print(Mathf.PerlinNoise(5f, 2f));
		generateTerrain ();

        //Ponemos al jugador en medio del mapa
        player.transform.position = new Vector3(cols/2, 30, rows/2);

        //Calculamos la diferencia del terreno
        difference = player.transform.position - this.transform.position;
        difference.y = 0;

        difference.x = Mathf.Floor(difference.x);
        difference.z = Mathf.Floor(difference.z);

        playerPos.x = Mathf.Floor(player.transform.position.x);
        playerPos.z = Mathf.Floor(player.transform.position.z);
        playerPos.y = 0;

        terrainPos = playerPos - difference;
        this.transform.position = terrainPos;

	}

    private void Update()
    {
        //Actualizamos la posicion del jugador
        playerPos.x = Mathf.Floor(player.transform.position.x);
        playerPos.z = Mathf.Floor(player.transform.position.z);
        playerPos.y = 0;

        //Actualizamos la posicion del terreno
        terrainPos = playerPos - difference;
        this.transform.position = terrainPos;

        
        //generateTerrain();

    }



    void generateTerrain(){

        //determinas posicion inicial
		myPos = this.transform.position;

        //dices numero de filas y columnas
		cols = 70;
		rows = 70;

        //recorro el mapa como si fuera una matriz.
		for (int x = 0; x < cols; x++) {


			for (int z = 0; z < rows; z++) {

                for (int a=-3; a>=-7;a--)
                {
                    float y = Mathf.PerlinNoise((myPos.x + x + seed + this.transform.position.x) / freq,
                    (myPos.z + z + this.transform.position.z) / freq) * amp;

                    //Instanciamos piedra cuando el mapa de perlin nos devuelva un numero superior o igual a 0.5 para crear galerias.
                    if (y >= 0.55f)
                    {
                        //Random para determinar el tipo de mineral que se instanciará. Por defecto la piedra
                        float minerales = Random.Range(0f, 100f);
                        int tipoMineral;
                        if (minerales > 80f && minerales <= 83f) //carbon
                        {
                            tipoMineral = 1;
                        }
                        else if (minerales > 85f && minerales <= 87f) //hierro
                        {
                            tipoMineral = 2;
                        }
                        else //Todo lo demas, piedra
                        {
                            tipoMineral = 0;
                        }
                        
                        GameObject newBlock = GameObject.Instantiate(blockTypes[tipoMineral]);
                        newBlock.transform.position =
                        new Vector3(myPos.x + x, a, myPos.z + z);
                    } else
                    {
                        //Rellenar el suelo con piedra
                        if (a==-7 || x==0 || z==0 || z==rows-1 || x==cols-1)
                        {
                            GameObject newBlock = GameObject.Instantiate(blockTypes[0]);
                            newBlock.transform.position =
                            new Vector3(myPos.x + x, a, myPos.z + z);
                        }
                    }



                }
                                
                
            }

		}

	
	}


}
