﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerController : MonoBehaviour
{    
    

    //Animator del player
    public Animator animator;
 
    //Booleano para andar
    bool move=true;

    //Booleano para detectar si esta andando
    bool moving = false;

    //Booleano para controlar si toca el suelo
    bool isGrounded = true;

    //Booleano para controlar la activacion de los triggers cuando pasas de agachado a levantado
    bool agachado = false;

    //Delegado para la barra de vida
    public delegate void recibirDaño();
    public event recibirDaño eventRecibirDaño;

    //Booleano para controlar si muere
    bool isDead = false;
    private float vida = 1;

    //Booleano para controlar si esta bloqueando
    private bool blocking = false;

    //Corrutina    
    private bool cBreakPuño1=false;
    private bool cBreakPuño2 = false;

    //Booleano barril
    bool grabBarrel=false;
    bool grabbingBarrel = false;
    GameObject barrel;

    bool gettingKicked=false;

    //Booleano para controlar el movimiento al alejarse de la camara
    bool limitCamara = false;

    void Start()
    {
        //Suscribirse delegados

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!blocking)
        {
            if (collision.tag == "puño1")
            {
                gettingKicked = true;
                this.GetComponent<Rigidbody2D>().velocity = new Vector3(-1, 0, 0);
                animator.SetTrigger("golpeado");
                vida -= 0.05f;
                if (eventRecibirDaño != null)
                {
                    eventRecibirDaño();
                }
            }
            else if (collision.tag == "puño2")
            {
                gettingKicked = true;
                this.GetComponent<Rigidbody2D>().velocity = new Vector3(-1, 0, 0);
                animator.SetTrigger("golpeado");
                vida -= 0.05f;
                if (eventRecibirDaño != null)
                {
                    eventRecibirDaño();
                }

            }
            else if (collision.tag == "puño3")
            {
                gettingKicked = true;
                this.GetComponent<Rigidbody2D>().AddForce(Vector2.up * 10, ForceMode2D.Impulse);
                animator.SetTrigger("golpeado");
                vida -= 0.05f;
                if (eventRecibirDaño != null)
                {
                    eventRecibirDaño();
                }

            }
        }

        if (collision.tag == "patada1")
        {
            if (!blocking)
            {
                if (eventRecibirDaño != null)
                {
                    eventRecibirDaño();
                }

                if (isGrounded)
                {
                    gettingKicked = true;
                    this.GetComponent<Rigidbody2D>().velocity = new Vector3(-1, 0, 0);
                    animator.SetTrigger("golpeado");
                    vida -= 0.05f;
                }
                else
                {
                    gettingKicked = true;
                    animator.SetBool("golpeadoAire", true);
                    vida -= 0.05f;
                    this.GetComponent<Rigidbody2D>().velocity = new Vector3(-3, 5, 0);

                }
            }

        }
        

        if (collision.tag == "bolaHadouken")
        {
            if (!blocking)
            {
                animator.SetTrigger("golpeado");
                vida -= 0.05f;
                if (eventRecibirDaño != null)
                {
                    eventRecibirDaño();
                }
            }


        }

        if (collision.tag == "puñoAgachado" || collision.tag == "patadaAgachado")
        {
            if (!blocking)
            {
                animator.SetTrigger("golpeado");
                vida -= 0.05f;
                this.GetComponent<Rigidbody2D>().velocity = new Vector3(-1, 0, 0);
                if (eventRecibirDaño != null)
                {
                    eventRecibirDaño();
                }
            }


        }

        if (collision.tag == "bolaHadouken")
        {
            if (!blocking)
            {
                animator.SetTrigger("golpeado");
                vida -= 0.05f;
                if (eventRecibirDaño != null)
                {
                    eventRecibirDaño();
                }
            }


        }

        //Recibir hadouken cargado
        if (collision.tag == "hadoukenCargado")
        {
            if (!blocking)
            {
                animator.SetTrigger("golpeado");
                vida -= 0.05f;
                if (eventRecibirDaño != null)
                {
                    eventRecibirDaño();
                }
            }


        }

        if (collision.tag=="barrel")
        {
            grabBarrel = true;
            barrel = collision.gameObject;
        } else
        {
            grabBarrel = false;
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (isGrounded)
        {
            gettingKicked = false;
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "suelo")
        {
            gettingKicked = false;
            isGrounded = true;            
        }        
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.transform.tag == "suelo")
        {
            
            isGrounded = false;
        }
    }

    void Update()
    {
        

        if (isGrounded)
        {
            animator.SetBool("golpeadoAire", false);
        }

        if (vida <= 0)
        {
            animator.SetBool("golpeadoAire", true);
            isDead = true;
        }

        

        //Andar hacia adelante y hacia atras
        if (!isDead)
        {
        if (Input.GetKey("d") && this.transform.position.x < 25.4 && move)
        {
            animator.SetBool("walk", true);
            //this.gameObject.transform.position = new Vector2(this.transform.position.x + vel, this.transform.position.y);  
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(8,this.GetComponent<Rigidbody2D>().velocity.y);
            moving=true;

        }
        else if(Input.GetKey("a") && this.transform.position.x > -15 && move && limitCamara)
        {
            
            animator.SetBool("walk", true);
            //this.gameObject.transform.position = new Vector2(this.transform.position.x - vel, this.transform.position.y);            
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-8, this.GetComponent<Rigidbody2D>().velocity.y );
            moving =true;

            
        } else
        {
            if(!gettingKicked){
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
            }
            
            
            animator.SetBool("walk", false);
            moving=false;
        }
        }
       



        
        //condiciones para andar
        if (animator.GetBool("bloquear").Equals(false) && animator.GetBool("agachado").Equals(false))
        {
            move = true;
        } else
        {
            move = false;
        }

        if (!gameController.limiteCamara)
        {
            limitCamara = true;
        }
        else
        {
            limitCamara = false;
        }

        //Agacharse
        if (!grabbingBarrel)
        {
            if (Input.GetKey("s"))
            {
                animator.SetBool("agachado", true);
                agachado = true;

                if (Input.GetKey("b"))
                {
                    //Bloquear agachado
                    animator.SetBool("bloquearAgachado", true);

                }
                else if (Input.GetKeyDown("x"))
                {
                    //Puñetado agachado
                    animator.SetTrigger("puñoAgachado");

                }
                else if (Input.GetKeyDown("v"))
                {
                    //Patada agachado
                    animator.SetTrigger("patadaAgachado");

                }
                else
                {
                    animator.SetBool("bloquearAgachado", false);
                }
            }
            else
            {
                animator.SetBool("agachado", false);
                agachado = false;
            }
        }
                
       

        //Saltar
        if (Input.GetKeyDown("w") && isGrounded && !grabbingBarrel)
        {      
            this.GetComponent<Rigidbody2D>().AddForce(Vector2.up * 15f, ForceMode2D.Impulse);
        }



        if (isGrounded)
        {
            animator.SetBool("salto", false);
        } else
        {
            animator.SetBool("salto", true);
        }

        //Bloquear
        if (!grabbingBarrel)
        {
            if (Input.GetKey("b"))
            {
                animator.SetBool("bloquear", true);
            }
            else
            {
                animator.SetBool("bloquear", false);
            }

            if (grabBarrel)
            {
                if (Input.GetKeyDown("z"))
                {
                    if (!grabbingBarrel)
                    {
                        grabbingBarrel = true;
                        
                    } 
                    
                }
            }            
        }

        //Soltar el barril
        if (grabbingBarrel && Input.GetKeyDown("space"))
        {
            grabbingBarrel = false;
            grabBarrel = false;
            barrel.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
            barrel.GetComponent<Rigidbody2D>().velocity = new Vector3(10, 5, 0);
        }

        //Coger el barril
        if (grabbingBarrel)
        {
            barrel.transform.position = new Vector3(this.transform.position.x + 1, this.transform.position.y + 1, this.transform.position.z);           
        } 


        //Golpes
        if (!agachado && !moving && !grabbingBarrel)
        {            
            //Puñetazo 1
            if (Input.GetKeyDown("x") && !cBreakPuño1 && !cBreakPuño2)
            {                
                animator.SetTrigger("puño1");                
                StartCoroutine(comboPuños(1));
            }

            //puñetazo 2
            if (Input.GetKeyDown("c") && cBreakPuño1)
            {               
                animator.SetTrigger("puño2");
                StartCoroutine(comboPuños(2));
            }

            //puñetazo 3
            if (Input.GetKeyDown("x") && cBreakPuño2)
            {                
                animator.SetTrigger("puño3");
            }


            //Patada 1
            if (Input.GetKeyDown("v"))
            {                
                animator.SetTrigger("patada1");               
            } 

            //hadouken provisional
            if (Input.GetKeyDown("h"))
            {
                animator.Play("hadouken");
            }
        }
        
    }
    
    IEnumerator comboPuños(int num)
    {
        if (num==1)
        {
            cBreakPuño1 = true;
            yield return new WaitForSeconds(0.2f);
            cBreakPuño1 = false;

        } else if (num == 2)
        {
            cBreakPuño2 = true;
            yield return new WaitForSeconds(0.2f);
            cBreakPuño2 = false;
        } 
    }
   
    public void recibirHadoukenCargado()
    {

    }

}
