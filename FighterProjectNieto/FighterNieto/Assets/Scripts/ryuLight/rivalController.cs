﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rivalController : MonoBehaviour
{      

    //Animator del player
    public Animator animator;

   
    //Booleano para andar
    bool move = true;

    //Booleano para detectar si esta andando
    bool moving = false;

    //Booleano para controlar si toca el suelo
    bool isGrounded = true;

    //Booleano para controlar la activacion de los triggers cuando pasas de agachado a levantado
    bool agachado = false;

    //Delegado para la barra de vida
    public delegate void recibirDañoRival(float daño);
    public event recibirDañoRival eventRecibirDañoRival;

    //Delegado para recibir el ataque cargado
   

    //Booleano para controlar si muere
    bool isDead = false;
    private float vida = 1;

    //Booleano para controlar si esta bloqueando
    private bool blocking = false;

    //Corrutina    
    private bool cBreakPuño1 = false;
    private bool cBreakPuño2 = false;

    //Booleano barril
    bool grabBarrel = false;
    bool grabbingBarrel = false;
    GameObject barrel;

     //Boleano me estan pegando
     bool gettingKicked=false;
      
    public GameObject ryuHeavy;

    //Booleano para controlar el movimiento al alejarse de la camara
    bool limitCamara = false;

    void Start()
    {
        
    }

    //TriggerEnter para recibir los golpes y llamar a la barra de vida
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Recibir puñetazos
        if (!blocking)
        {
            if (collision.tag == "puño1")
            {
                
                gettingKicked=true;
                this.GetComponent<Rigidbody2D>().velocity = new Vector3(1, 0, 0);                
                animator.SetTrigger("golpeado");
                vida -= 0.05f;
                if (eventRecibirDañoRival != null)
                {
                    eventRecibirDañoRival(0.05f);
                }
            }
            else if (collision.tag == "puño2")
            {
                gettingKicked = true;
                this.GetComponent<Rigidbody2D>().velocity = new Vector3(1, 0, 0);                
                animator.SetTrigger("golpeado");
                vida -= 0.05f;
                if (eventRecibirDañoRival != null)
                {
                    eventRecibirDañoRival(0.05f);
                }

            }
            else if (collision.tag == "puño3")
            {
                gettingKicked = true;
                this.GetComponent<Rigidbody2D>().AddForce(Vector2.up * 10, ForceMode2D.Impulse);
                animator.SetTrigger("golpeado");
                vida -= 0.1f;

                if (eventRecibirDañoRival != null)
                {
                    eventRecibirDañoRival(0.1f);
                }

            } else if (collision.tag == "puño3Heavy")
            {
                gettingKicked = true;
                this.GetComponent<Rigidbody2D>().AddForce(Vector2.up * 20, ForceMode2D.Impulse);
                animator.SetTrigger("golpeado");
                vida -= 0.1f;

                if (eventRecibirDañoRival != null)
                {
                    eventRecibirDañoRival(0.1f);
                }
            }
        }

        //Recibir hadouken cargado
        if (!blocking)
        {
            if (collision.tag == "bolaHeavy1")
            {

                gettingKicked = true;
                this.GetComponent<Rigidbody2D>().velocity = new Vector3(1, 0, 0);
                animator.SetTrigger("golpeado");
                vida -= 0.05f;
                if (eventRecibirDañoRival != null)
                {
                    eventRecibirDañoRival(0.05f);
                }
            }
            else if (collision.tag == "bolaHeavy2")
            {
                gettingKicked = true;
                this.GetComponent<Rigidbody2D>().velocity = new Vector3(1, 0, 0);
                animator.SetTrigger("golpeado");
                vida -= 0.15f;
                if (eventRecibirDañoRival != null)
                {
                    eventRecibirDañoRival(0.15f);
                }

            }
            
        }

        //Recibir patada
        if (collision.tag == "patada1")
        {
            if (!blocking)
            {
                if (eventRecibirDañoRival != null)
                {
                    eventRecibirDañoRival(0.05f);
                }

                if (isGrounded)
                {
                    gettingKicked = true;
                    this.GetComponent<Rigidbody2D>().velocity = new Vector3(1, 0, 0);
                    animator.SetTrigger("golpeado");
                    vida -= 0.05f;
                }
                else
                {
                    gettingKicked = true;
                    this.GetComponent<Rigidbody2D>().velocity = new Vector3(6, 5, 0);
                    animator.SetBool("golpeadoAire", true);
                    vida -= 0.05f;     
                                  

                }                
            }
        } 

        
        //Recibir hadouken
        if (collision.tag == "bolaHadouken")
        {
            if (!blocking)
            {
                animator.SetTrigger("golpeado");
                vida -= 0.05f;
                

            }


        }

        //Recibir golpes bajos
        if (collision.tag == "puñoAgachado" || collision.tag == "patadaAgachado")
        {
            if (!blocking)
            {
                gettingKicked = true;
                animator.SetTrigger("golpeado");
                vida -= 0.05f;
                this.GetComponent<Rigidbody2D>().velocity = new Vector3(1, 0, 0);
                if (eventRecibirDañoRival != null)
                {
                    eventRecibirDañoRival(0.05f);
                }
            }


        }

        //Recibir barrilazo
        if (collision.tag == "barrel")
        {
            animator.SetTrigger("golpeado");
            vida -= 0.05f;
            grabBarrel = true;
            barrel = collision.gameObject;
            if (eventRecibirDañoRival != null)
                {
                    eventRecibirDañoRival(0.05f);
                }
        }
        else
        {
            grabBarrel = false;
        }

        

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (isGrounded)
        {
            gettingKicked = false;
        }
            
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "suelo")
        {
            isGrounded = true;
            gettingKicked = false;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.transform.tag == "suelo")
        {
            isGrounded = false;
        }
    }

    void Update()
    {
        

        if (isGrounded)
        {           
            animator.SetBool("golpeadoAire",false);            
        }

        if (vida<=0)
        {
            animator.SetBool("golpeadoAire", true);
            isDead = true;
        }

        

        //Andar hacia adelante y hacia atras
        if (!isDead)
        {
            if (Input.GetKey("l") && move && limitCamara)
            {
                animator.SetBool("walk", true);                
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(8,this.GetComponent<Rigidbody2D>().velocity.y);                
                moving = true;
            }
            else if (Input.GetKey("j") && move)
            {
                
                animator.SetBool("walk", true);
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(-8, this.GetComponent<Rigidbody2D>().velocity.y);
                moving = true;
            }
            else
            {

                if (!gettingKicked)
                {
                    this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
                }
                
                animator.SetBool("walk", false);
                moving = false;
                
            }
        }
       


        //condiciones para andar
        if (animator.GetBool("bloquear").Equals(false) && animator.GetBool("agachado").Equals(false))
        {
            move = true;
        }
        else
        {
            move = false;
        }

        if (!gameController.limiteCamara)
        {
            limitCamara = true;
        }
        else
        {
            limitCamara = false;
        }

        //Agacharse        
        if (Input.GetKey("k"))
            {
                animator.SetBool("agachado", true);
                agachado = true;

                if (Input.GetKey(KeyCode.Keypad6))
                {
                    //Bloquear agachado
                    animator.SetBool("bloquearAgachado", true);

                }
                else if (Input.GetKeyDown(KeyCode.Keypad7))
                {
                    //Puñetado agachado
                    animator.SetTrigger("puñoAgachado");

                }
                else if (Input.GetKeyDown(KeyCode.Keypad9))
                {
                    //Patada agachado
                    animator.SetTrigger("patadaAgachado");

                }
                else
                {
                    animator.SetBool("bloquearAgachado", false);
                }
            }
            else
            {
                animator.SetBool("agachado", false);
                agachado = false;
            }
        



        //Saltar
        if (Input.GetKeyDown("i") && isGrounded && !grabbingBarrel)
        {      
            this.GetComponent<Rigidbody2D>().AddForce(Vector2.up * 15f, ForceMode2D.Impulse);
        }

        //Actualizar el salto
        if (isGrounded)
        {
            animator.SetBool("salto", false);
        }
        else
        {
            animator.SetBool("salto", true);
        }

        //Bloquear
        if (!grabbingBarrel)
        {
            if (Input.GetKey(KeyCode.Keypad6))
            {
                animator.SetBool("bloquear", true);
                blocking = true;
            }
            else
            {
                animator.SetBool("bloquear", false);
                blocking = false;
            }

           
        }

        
              

        //Golpes
        if (!agachado && !moving && !grabbingBarrel)
        {
            //Puñetazo 1
            if (Input.GetKeyDown(KeyCode.Keypad7) && !cBreakPuño1 && !cBreakPuño2)
            {
                animator.SetTrigger("puño1");
                StartCoroutine(comboPuños(1));
            }

            //puñetazo 2
            if (Input.GetKeyDown(KeyCode.Keypad8) && cBreakPuño1)
            {
                animator.SetTrigger("puño2");
                StartCoroutine(comboPuños(2));
            }

            //puñetazo 3
            if (Input.GetKeyDown(KeyCode.Keypad7) && cBreakPuño2)
            {
                animator.SetTrigger("puño3");
            }


            //Patada 1
            if (Input.GetKeyDown(KeyCode.Keypad9))
            {
                animator.SetTrigger("patada1");
            }

            //hadouken 
            if (Input.GetKeyDown(KeyCode.Keypad5))
            {
                animator.Play("hadouken");
            }

            //Debug.Log(vida);
        }

    }

    IEnumerator comboPuños(int num)
    {
        if (num == 1)
        {
            cBreakPuño1 = true;
            yield return new WaitForSeconds(0.2f);
            cBreakPuño1 = false;

        }
        else if (num == 2)
        {
            cBreakPuño2 = true;
            yield return new WaitForSeconds(0.2f);
            cBreakPuño2 = false;
        }
    }
    
}
