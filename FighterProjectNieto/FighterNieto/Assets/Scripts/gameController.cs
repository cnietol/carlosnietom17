﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class gameController : MonoBehaviour
{
    //Camara
    public Camera camara;

    //Vida Player
    public Image healthbarPlayer;
    private float healthPlayer;

    //Vida Rival
    public Image healthbarRival;
    private float healthRival;
        

    //Arrays 
    public Sprite[] backgrounds;
    public AudioClip[] canciones;
    public AudioSource audiosource;
    public GameObject[] personajes;

    //Cargar personajes indicados
    public GameObject p1;
    public GameObject p2;

    //controlador de escenas
    public GameObject controlador;

    //Control de los jugadores
    public static bool limiteCamara = false;
    


    // Start is called before the first frame update
    void Start()
    {

        
        
        //Instanciar personajes segun el character select
        if (PlayerPrefs.GetString("player1") == "ryu")
        {
            p1 = personajes[0];
            p1 = Instantiate(p1);
            //Suscribirse delegados
            p1.GetComponent<playerController>().eventRecibirDaño += recibirDaño;

        } else if (PlayerPrefs.GetString("player1") == "ryuHeavy")
        {
            p1 = personajes[2];
            p1 = Instantiate(p1);
            p1.GetComponent<ryuHeavyController>().eventRecibirDaño += recibirDaño;
            p1.GetComponent<SpriteRenderer>().color = new Color(255/255f,168/255f,168/255f);
        }

        if (PlayerPrefs.GetString("player2") == "ryu")
        {
            p2 = personajes[1];
            p2 = Instantiate(p2);
            p2.GetComponent<rivalController>().eventRecibirDañoRival += recibirDañoRival;
        }


        //Vida
        healthPlayer = 1;
        healthRival = 1;
        
        
        //Escenario con su respectiva musica
        
        if (PlayerPrefs.GetString("escenario")=="japon")
        {
            controlador.transform.GetChild(1).gameObject.SetActive(true);
            audiosource.PlayOneShot(canciones[0]);

        } else if (PlayerPrefs.GetString("escenario") == "usa")
        {
            controlador.transform.GetChild(0).gameObject.SetActive(true);
            audiosource.PlayOneShot(canciones[1]);

        }

        


    }

    // Update is called once per frame
    void Update()
    {
   
       

        //Camara
        float Distx = Mathf.Abs(p1.transform.position.x - p2.transform.position.x);
        

        if (Distx> 19)
        {
            limiteCamara = true;
        } else
        {
            limiteCamara = false;
        }
        
        
        
        camara.transform.localPosition = new Vector3((p1.transform.position.x+p2.transform.position.x)/2, 0, -10f);
        
       
      

    }

    //Funcion llamada por el delegado al recibir daño
    public void recibirDaño()
    {
        healthPlayer -= 0.05f;
        healthbarPlayer.fillAmount = healthPlayer;
    }

    public void recibirDañoRival(float daño)
    {
        healthRival -= daño;
        healthbarRival.fillAmount = healthRival;
    }


}
