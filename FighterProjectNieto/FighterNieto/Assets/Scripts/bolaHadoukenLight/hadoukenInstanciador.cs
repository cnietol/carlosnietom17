﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hadoukenInstanciador : MonoBehaviour
{

    public GameObject hadouken;    
    public GameObject hadoukenPrefab;
    public GameObject player;
    public bool hadoukenInstanciado;

    public GameObject hadoukenControlador;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       
        if (hadouken.activeSelf==true && !hadoukenInstanciado)
        {
            GameObject a = Instantiate(hadoukenPrefab) as GameObject;
            a.transform.position = new Vector3(player.transform.position.x+4, player.transform.position.y, player.transform.position.z);
            hadoukenInstanciado = true;           
        } else if (hadouken.activeSelf == false && hadoukenInstanciado)
        {
            hadoukenInstanciado = false;
        }

        

    }

    
}
