﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events; // 1

public class GameEventListener : MonoBehaviour
{
    [SerializeField]
    private GameEvent gameEvent; // el GameEvent (classe propia) al que ens suscribim
    [SerializeField]
    private UnityEvent response; // el UnityEvent (qualsevol event d'unity) que donem com a resposta

    private void OnEnable() // 4
    {
        gameEvent.SubscribeListener(this);
    }

    private void OnDisable() // 5
    {
        gameEvent.UnSubscribeListener(this);
    }

    public void OnEventRaised() // aquesta funcio es cridara al ser cridat un event
    {
        response.Invoke();
    }
}
