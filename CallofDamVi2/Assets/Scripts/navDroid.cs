﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class navDroid : MonoBehaviour
{
    #region GameObjects
    [Header("gameObjects")]
    public Droid droid;
    private GameObject player;
    #endregion

    #region NavMesh
    [Header("Nav Agent")]
    public NavMeshAgent agent;
    #endregion

    private GameObject[] players;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (player == null)
        {
            this.players = GameObject.FindGameObjectsWithTag("player");
            int p = Random.Range(0, players.Length);
            print(p);
            this.player = players[p];        
            
        }

        if (0 < droid.hp)
        {
            this.transform.LookAt(player.transform);
            //Seteas el objetivo al que quieres que baya
            agent.SetDestination(player.transform.position);
        }
        else
        {
            this.agent.enabled = false;
        }
    }
}
