﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Game Event", menuName = "Game Event", order = 53)] // 1
public class GameEvent : ScriptableObject // Scriptable Object
{
    #region Variables
    private List<GameEventListener> listeners = new List<GameEventListener>(); // Tots els listeners d'aquest event
    #endregion

    #region Metodos
    public void Raise() // quan es crida l'event
    {
        foreach (GameEventListener gel in listeners)
        {
            gel.OnEventRaised(); //cridem a tots els seus listeners
        }
    }

    public void SubscribeListener(GameEventListener listener) //suscriure un listener
    {
        listeners.Add(listener);
    }

    public void UnSubscribeListener(GameEventListener listener) //desuscriure un listener
    {
        listeners.Remove(listener);
    }
    #endregion

}
