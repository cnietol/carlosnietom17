﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class disparoAK : MonoBehaviour
{

     public Camera cam;
    public GameObject impacto;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            //Declara el hit
            RaycastHit hit;
            //Agafar el Vector 3 de la camera
            Vector3 forward = cam.transform.TransformDirection(Vector3.forward) * 1000;
            //Dibuixar el raig del RayCast
            Debug.DrawRay(transform.position, forward, Color.green);

            if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hit))
            {
                print(hit.transform.name);
                //Agafa que el target es el player
                barril barrilete = hit.transform.GetComponent<barril>();
                print(barrilete);
                //Instancia el dispar
                GameObject shoot = Instantiate(impacto, hit.point, Quaternion.LookRotation(hit.normal));
                shoot.transform.LookAt(hit.point);
                //StartCoroutine(finImpacto(shoot));
                if (barrilete != null)
                {
                    print("entra");
                    //Baixa la vida del player
                    barrilete.gethit = true;
                }
            }

        }
    }
}
