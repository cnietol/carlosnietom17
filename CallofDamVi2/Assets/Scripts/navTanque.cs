﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class navTanque : MonoBehaviour
{
    #region NavMesh
    [Header("Nav Agent")]
    public NavMeshAgent agent;
    #endregion

    #region Transforms
    [Header("Coordenadas")]
    public Transform cruze1;
    public Transform cruze2;
    public Transform cruze3;
    public Transform cruze4;
    #endregion

    #region Variables
    // private int cruze;
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        // cruze = 1;
        agent.SetDestination(cruze1.transform.position);
    }

    // Update is called once per frame
    void Update()
    {
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Cruze1")
        {
            agent.SetDestination(cruze4.transform.position);

        }
        else if (other.gameObject.name == "Cruze2")
        {
            agent.SetDestination(cruze1.transform.position);

        }
        else if (other.gameObject.name == "Cruze3")
        {
            agent.SetDestination(cruze2.transform.position);

        }
        else if (other.gameObject.name == "Cruze4")
        {
            agent.SetDestination(cruze3.transform.position);
        }
    }
}
