﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroudonF : MonoBehaviour
{
    public float hp, atac, def, Lv;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //Script para coger los valores de groudon i destruirlo cuando le ganas
    }
    public float gethp()
    {
        return hp;
    }
    public float getLv()
    {
        return Lv;
    }
    public float getatac()
    {
        return atac;
    }
    public float getdef()
    {
        return def;
    }
    public void destroy(){
        Destroy(this);
        this.enabled=false;
    }
}
