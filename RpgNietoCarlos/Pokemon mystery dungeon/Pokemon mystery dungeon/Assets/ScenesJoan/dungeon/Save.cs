﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Save
{    
        //Clase que se utiliza para serializar las variables que queremos guardar y cargar.
        public int dineroPlayerSave;
        public int dineroBancoSave, hpM;
        public float hpS, ataqueS, defensaS, levelS;

}
