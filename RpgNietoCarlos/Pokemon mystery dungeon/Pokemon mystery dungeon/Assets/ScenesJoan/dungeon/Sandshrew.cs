﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Sandshrew : MonoBehaviour
{
    public Animator animator;
    private GameObject Player;
    private Random Random = new Random();
    public Sandshrew Enemy;
    private bool trigger;
    private static bool triggerfight;
    private float DistX, DistY;
    public float MinDist;
    private int move;
    public int vel;
  
    // Start is called before the first frame update
    void Start()
    {
        Player = GameObject.Find("Player");
        animator = gameObject.GetComponent<Animator>();
        //Se invoca Move a los 2 segundos al empezar, y despues cada 0.5sec.
        InvokeRepeating("Move", 2, 0.5f);
    }

    // Update is called once per frame
    void Update()
    {
        //Atualizamos la posicion de sandshrew 

        Enemy.transform.position = new Vector3(Enemy.transform.position.x,
        Enemy.transform.position.y,
        Enemy.transform.position.z);

        //Calculo la distancia que hay entre el player i groudon
        DistX = Enemy.transform.position.x - Player.transform.position.x;
        DistY = Enemy.transform.position.y - Player.transform.position.y;
    }

    void Hunt()
    {
        //Caluculando las distancia, se mueve de una manera o de otra, solo se mueve en diagonal, 
        //porque es muy dificil coincidir el awake, con la posicion 0.

        if (DistX <= MinDist)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);
            animator.SetBool("MoveR", true);
            animator.SetBool("MoveB", false);
        }
        else if (DistX >= MinDist)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, this.GetComponent<Rigidbody2D>().velocity.y);
            animator.SetBool("MoveB", true);
            animator.SetBool("MoveR", false);
        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
            animator.SetBool("MoveR", false);
            animator.SetBool("MoveB", false);
        }
        if (DistY <= MinDist)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, vel);
            animator.SetBool("MoveU", true);
            animator.SetBool("MoveD", false);

        }
        else if (DistY >= MinDist)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, -vel);
            animator.SetBool("MoveD", true);
            animator.SetBool("MoveU", false);

        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, 0);
        }

    }
    void Move()
    {
        //Movimiento aleatorio del enemigo mietras no este hunt activado
        trigger = GameObject.Find("Player").GetComponent<Player>().getTrigger();

        if (trigger)
        {
            Hunt();

            //Si la distancia entre los 2 es menor a 2 entra en combate
            if (Vector3.Distance(Enemy.transform.position, Player.transform.position) <= 2)
            {
                triggerfight = true;
                SceneManager.LoadScene("SandshrewFight");
            }
            else
            {
                triggerfight = false;
            }
        }
        else
        {

            move = Random.Range(1, 5);

            if (move == 1)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);
                animator.SetBool("MoveR", true);
                animator.SetBool("MoveB", false);
            }
            else if (move == 2)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, this.GetComponent<Rigidbody2D>().velocity.y);
                animator.SetBool("MoveB", true);
                animator.SetBool("MoveR", false);
            }
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
                animator.SetBool("MoveR", false);
                animator.SetBool("MoveB", false);
            }
            if (move == 3)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, vel);
                animator.SetBool("MoveU", true);
                animator.SetBool("MoveD", false);

            }
            else if (move == 4)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, -vel);
                animator.SetBool("MoveD", true);
                animator.SetBool("MoveU", false);

            }
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, 0);
                animator.SetBool("MoveD", false);
                animator.SetBool("MoveU", false);

            }
        }
    }
    public static bool getFight()
    {
        return triggerfight;
    }
    public static void setFight()
    {
        triggerfight = false;
    }
}
