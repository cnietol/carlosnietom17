﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dialogos : MonoBehaviour
{
    //Booleanos para activar los dialogos
    public static bool hablandoAmpharos = false;
    public static bool hablandoOnyx = false;
    public static bool hablandoGrovyle = false;
    public static bool hablandoKangaskhan = false;
    public static bool puntoGuardado = false;
    public static bool entradaDungeon = false;

    // Start is called before the first frame update

    //Al entrar en el collider se activan los booleanos
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag=="DialogoAmpharos")
        {
            hablandoAmpharos = true;

        } else if (other.tag == "DialogoGrovyle")
        {
            hablandoGrovyle=true;
            
        }
        else if (other.tag == "DialogoOnyx")
        {
            hablandoOnyx = true;

        } else if (other.tag == "DialogoKangaskhan")
        {
            hablandoKangaskhan = true;
            
        } else if (other.tag == "EntradaDungeon")
        {
           entradaDungeon = true;
        }
        else if (other.tag == "puntoGuardado")
        {
            puntoGuardado = true;
        }
    }

    //Al salir del collider se desactivan los booleanos
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "DialogoAmpharos")
        {
            hablandoAmpharos = false;

        }
        else if (other.tag == "DialogoGrovyle")
        {
             hablandoGrovyle=false;
        }
        else if (other.tag == "DialogoOnyx")
        {
            hablandoOnyx = false;
        }
        else if (other.tag == "DialogoKangaskhan")
        {
            hablandoKangaskhan = false;
        } else if(other.tag == "EntradaDungeon"){
            entradaDungeon = false;
        } else if (other.tag == "puntoGuardado")
        {
            puntoGuardado = false;
        }


    }


    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
}
