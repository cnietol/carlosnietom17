﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlM : MonoBehaviour
{
    private bool trigger;
    public Player player;
    public Camera camera;
    public GameObject roca;
    
    bool romperRoca=false;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        camera.transform.position = new Vector3(
           player.transform.position.x,
           player.transform.position.y,
           camera.transform.position.z);

        trigger = GameObject.Find("Player").GetComponent<Player>().getTriggerMF();


        if (Input.GetKeyDown(KeyCode.Return))
        {

            //Canvas.SetEnable(true);
            SceneManager.LoadScene("Carlos");

        }
        else
        {
            ///Canvas.SetEnable(false);
        }

        if (trigger)
        {
            SceneManager.LoadScene("Groudon");
        }

    }

    private void OnTriggerEnter2D(Collider2D other) {
        
        if(other.gameObject.name=="Roca"){
            romperRoca=true;
        }
        
    }
}
