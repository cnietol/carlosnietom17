﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Control : MonoBehaviour
{
    private bool triggerH, triggerF;
    public Player player;
    public Camera camera;

    //Dinero del jugador
    public int dinero = 0;
    public int dineroBanco = 0;

    //Botones para el menu banco
    public Button suma;
    public Button resta;
    public Button accepta;

    //Imagenes para el avatar segun con quien hables
    public Sprite avatarAmpharos;
    public Sprite avatarOnyx;
    public Sprite avatarGrovyle;
    public Sprite avatarKangaskhan;

    //Para activar las conversaciones;
    public Canvas ventanaDialogo;

    public Text textoDialogo;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        camera.transform.position = new Vector3(
           player.transform.position.x,
           player.transform.position.y,
           camera.transform.position.z);

        triggerH = GameObject.Find("Player").GetComponent<Player>().getTriggerEB();
        triggerF = GameObject.Find("Player").GetComponent<Player>().getTriggerMF();

        if (Input.GetKeyDown(KeyCode.Return))
        {

            //Canvas.SetEnable(true);
            //SceneManager.LoadScene("Carlos");

        }
        else
        {
            ///Canvas.SetEnable(false);
        }

        if (triggerH)
        {
            SceneManager.LoadScene("HotDungeon");
        }
        if (triggerF)
        {
            SceneManager.LoadScene("Groudon");
        }


        //Dialogo con los NPC
        if (Dialogos.hablandoAmpharos)
        {
            if (Input.GetKeyDown("f"))
            {
                ventanaDialogo.GetComponent<Canvas>().enabled = true;
                ventanaDialogo.transform.GetChild(2).gameObject.SetActive(true);
                ventanaDialogo.transform.GetChild(1).gameObject.GetComponent<Image>().sprite = avatarAmpharos;
                textoDialogo.text = "Ampharos: Bienvenidos a la banca etica! Aqui no invertimos en Pokeguerras." +
                    "Tienes " + dineroBanco + " pokemoneda/s. Quires depositar o retirar dinero?";/*mejorar*/
            }
        }
        else if (Dialogos.hablandoOnyx)
        {
            if (Input.GetKeyDown("f"))
            {
                ventanaDialogo.GetComponent<Canvas>().enabled = true;
                ventanaDialogo.transform.GetChild(1).gameObject.GetComponent<Image>().sprite = avatarOnyx;
                textoDialogo.text = "Onyxillo: Que pasa mariconas? Recordad no tener relaciones con Pokemons del mismo genero";
            }


        }
        else if (Dialogos.hablandoGrovyle)
        {

            if (Input.GetKeyDown("f"))
            {
                ventanaDialogo.GetComponent<Canvas>().enabled = true;
                ventanaDialogo.transform.GetChild(3).gameObject.SetActive(true);
                ventanaDialogo.transform.GetChild(1).gameObject.GetComponent<Image>().sprite = avatarGrovyle;
                textoDialogo.text = "DroGAYle: Shh no hables tan fuerte, que necesitas?";

            }


        }
        else if (Dialogos.hablandoKangaskhan)
        {
            if (Input.GetKeyDown("f"))
            {
                ventanaDialogo.GetComponent<Canvas>().enabled = true;
                ventanaDialogo.transform.GetChild(2).gameObject.SetActive(true);
                ventanaDialogo.transform.GetChild(1).gameObject.GetComponent<Image>().sprite = avatarKangaskhan;
                textoDialogo.text = "Que duro es ser padre soltero... Bienvenido al deposito de objetos";

            }


        }
        else if (Dialogos.entradaDungeon)
        {
            ventanaDialogo.GetComponent<Canvas>().enabled = true;
            ventanaDialogo.transform.GetChild(1).gameObject.SetActive(false);
            ventanaDialogo.transform.GetChild(4).gameObject.SetActive(true);
            textoDialogo.text = "Deseas entrar a la mazmorra?";
            if (Input.GetKeyDown(KeyCode.Return))
            {
                SceneManager.LoadScene("Dungeon1");
            }

        }

        else
        {
            ventanaDialogo.GetComponent<Canvas>().enabled = false;
            ventanaDialogo.transform.GetChild(2).gameObject.SetActive(false);
            ventanaDialogo.transform.GetChild(1).gameObject.SetActive(true);
            ventanaDialogo.transform.GetChild(3).gameObject.SetActive(false);
            ventanaDialogo.transform.GetChild(4).gameObject.SetActive(false);
            ventanaDialogo.transform.GetChild(5).gameObject.SetActive(false);
           
        }
    }

    public void depositarDinero()
    {
        
        textoDialogo.text = "Cuanto quieres depositar? ";
        ventanaDialogo.transform.GetChild(5).gameObject.SetActive(true);
        
    }

    

}