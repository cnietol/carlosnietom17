﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Groudon : MonoBehaviour
{
    public int vel;
    public float MinDist;
    private int move;
    private float DistX, DistY;
    private bool trigger;
    public Animator animator;
    Random Random = new Random();
    public Transform Player;
    public Groudon Enemy;
    // Start is called before the first frame update
    void Start()
    {
        animator = gameObject.GetComponent<Animator>();
        InvokeRepeating("Hunt", 2, 1);
    }

    // Update is called once per frame
    void Update()
    {
        Enemy.transform.position = new Vector3(Enemy.transform.position.x,
        Enemy.transform.position.y,
        Enemy.transform.position.z);

        DistX = Enemy.transform.position.x - Player.position.x;
        DistY = Enemy.transform.position.y - Player.position.y;

       

    }

    void Hunt()
    {
        trigger = GameObject.Find("Player").GetComponent<Player>().getTriggerG();
        if (!trigger)
        {
            animator.SetBool("Awake", false);
        }
        else
        {
            animator.SetBool("Awake", true);
            if (DistX <= MinDist)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);
                animator.SetBool("MoveR", true);
                animator.SetBool("MoveB", false);
            }
            else if (DistX >= MinDist)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, this.GetComponent<Rigidbody2D>().velocity.y);
                animator.SetBool("MoveB", true);
                animator.SetBool("MoveR", false);
            }
            else
            {
                /* this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);*/
                animator.SetBool("MoveR", false);
                animator.SetBool("MoveB", false);
            }
            if (DistY <= MinDist)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, vel);
                animator.SetBool("MoveU", true);
                animator.SetBool("MoveD", false);

            }
            else if (DistY >= MinDist)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, -vel);
                animator.SetBool("MoveD", true);
                animator.SetBool("MoveU", false);

            }
            else
            {
                /*  this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, 0);*/
                animator.SetBool("MoveD", false);
                animator.SetBool("MoveU", false);

            }
            if (Vector3.Distance(Enemy.transform.position, Player.position) <= MinDist)
            {
                SceneManager.LoadScene("fight");
            }
        }
    }   
}
