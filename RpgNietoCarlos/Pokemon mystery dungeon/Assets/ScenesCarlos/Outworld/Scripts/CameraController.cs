﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject squirtle;
    // Start is called before the first frame update
    void Start()
    {
        this.transform.position = new Vector3(squirtle.transform.position.x, squirtle.transform.position.y, -1);
    }

    // Update is called once per frame
    void Update()
    {
        if (squirtle.transform.position.x > -2.38868 && squirtle.transform.position.x < 41.58463)
        {
            this.transform.position = new Vector3(squirtle.transform.position.x, squirtle.transform.position.y, -1);
        }
        
    }
}
